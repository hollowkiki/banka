<?php
//http://jqueryui.com/button/
session_start();

$user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';
$action = !empty($_GET['action']) ? $_GET['action'] : '';

if(!$_SESSION['logged']){
	header("Location: login_admin.php");
}
?>

<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/admin.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Dodaj klijenta</title>
	<script>
		function confirmation() {
			window.alert("Klijent je uspješno dodan u bazu podataka!");
		}
	</script>
</head>
<body>
<center>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
 	 <div class="container-fluid">
   	 <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      <a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-terminal'></i> ". $user . "</p><br><br> "; ?></p></a>


     </div>
     <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
				<li class='active'><a href='admin_dodaj.php'><i class="fa fa-check"></i> <span>Dodaj</span></a></li>
				<li><a href='admin_novi_zahtjevi.php'><i class="fa fa-user"></i> <span>Novi zahtjevi</span></a></li>
				<li><a href='admin_azuriraj.php'><i class="fa fa-wrench"></i> <span>Ažuriraj</span></a></li>
				<li><a href='admin_obrisi.php'><i class="fa fa-close"></i> <span>Obriši</span></a></li>
				<li><a href='admin_transakcije.php'><i class="fa fa-dollar"></i> <span>Transakcije</span></a></li>
				<li><a href='admin_poruka.php'><i class="fa fa-envelope"></i> <span>Pošalji poruku</span></a></li>

   	  </ul>
      <ul class="nav navbar-nav navbar-right">
          <li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
  	  </ul>
     </div>
     </div>
   </nav>
		<div id="mainContent">
			<br>
			<?php
				$con=@mysqli_connect("localhost","root","","banka");
				// check connection
				if (mysqli_connect_errno()) {
					printf("Konekcija neuspjela: %s\n", mysqli_connect_error());
					echo " <a href=klijent_landing.php>Povratak</a>";
					header('Content-Type: text/html; charset=utf-8');
				}

				if($action != 'Dodaj') {
				echo "<form action='admin_dodaj.php'>";
				echo "<p class='textich'><b>Želite li dodati novog klijenta u bazu?</b></p>";
				echo "<div style='padding-left: 30%;'><input type='submit' name='action' value='Dodaj'></div>";
				echo "</form>";
				} else {
					echo "<table>";
					echo "<form action='insert_klijent.php' onsubmit='confirmation()' method='POST'>";
					echo "<tr><td>OIB</td><td><input type='text' name='OIB'></td></tr>";
					echo "<tr><td>Ime</td><td><input type='text' name='Ime'></td></tr>";
					echo "<tr><td>Prezime</td><td><input type='text' name='Prezime'></td></tr>";
					echo "<tr><td>Datum rođenja</td><td><input type='text' name='Datum_rod'></td></tr>";
					echo "<tr><td>Prebivalište</td><td><input type='text' name='Mjesto_rod'></td></tr>";
					echo "<tr><td>Username</td><td><input type='text' name='username'></td></tr>";
					echo "<tr><td>Password</td><td><input type='text' name='password'></td></tr>";
					echo "<tr><td>Email</td><td><input type='text' name='email'></td></tr>";
					echo "</table>";
					echo "<input type='submit' name='button' value='Dodaj' style='float: right;'>";
					echo "</form>";
					}
			?>
		</div>
	</div>
	<!-- jQuery & Bootstrap javascript files -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>
