<?php
session_start();

$user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';

if(!$_SESSION['logged']){
	header("Location: login_admin.php");
}
$id = !empty($_GET['id']) ? $_GET['id'] : '';

?>

<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/admin.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Pošalji poruku</title>
	<script>
		function confirmation(){
			window.alert("Poruka je uspješno poslana!");
		}
	</script>
</head>
<body>
<center>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-terminal'></i> ". $user . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href='admin_dodaj.php'><i class="fa fa-check"></i> <span>Dodaj</span></a></li>
				<li><a href='admin_novi_zahtjevi.php'><i class="fa fa-user"></i> <span>Novi zahtjevi</span></a></li>
				<li><a href='admin_azuriraj.php'><i class="fa fa-wrench"></i> <span>Ažuriraj</span></a></li>
				<li><a href='admin_obrisi.php'><i class="fa fa-close"></i> <span>Obriši</span></a></li>
				<li><a href='admin_transakcije.php'><i class="fa fa-dollar"></i> <span>Transakcije</span></a></li>
				<li class='active'><a href='admin_poruka.php'><i class="fa fa-envelope"></i> <span>Pošalji poruku</span></a></li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
					<li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
	 </nav>
		<div id="mainContent">
			<br>
			<?php
				include("includes/mysql_con.php");
				$result = mysqli_query($con, "SELECT * FROM klijent WHERE status = 1 ORDER BY prezime,ime");

				if(!$id) {
					echo "<table>";
					echo "<tr><th>OIB</th><th>Ime</th><th>Prezime</th><th>Datum rođenja</th><th>Prebivalište</th><th>Username</th><th>Password</th><th>Email</th></tr>";

					while($row = mysqli_fetch_array($result)) {
						echo "<tr>";
						echo "<td>" . $row['OIB'] . "</td>";
						echo "<td>" . $row['Ime'] . "</td>";
						echo "<td>" . $row['Prezime'] . "</td>";
						echo "<td>" . $row['Datum_rod'] . "</td>";
						echo "<td>" . $row['Mjesto_rod'] . "</td>";
						echo "<td>" . $row['username'] . "</td>";
						echo "<td>" . $row['password'] . "</td>";
						echo "<td>" . $row['email'] . "</td>";
						echo "<td><a href='admin_poruka.php?id=". $row['id'] ."'><button class='btn btn-success'>POŠALJI</button></a></td>";
						echo "</tr>";
					}
					echo "</table>";
				} else {
					$_SESSION['id'] = $id;
					echo "<form action='posalji_poruku.php' onsubmit='confirmation()' method='POST' id='forma' name='forma'>";
					echo "<input type='text' name='title' placeholder='Naslov poruke..' style='padding: 5px; font-size: 12px;'><br>";
					echo "<textarea name='message' id='styled' form='forma'>Ovdje upišite poruku..</textarea></br>";
					echo "<input type='submit' name='button' value='Pošalji' style='float: right;'>";
					echo "</form>";
				}
				mysqli_close($con);
			?>
		</div>
	</div>
	<!-- jQuery & Bootstrap javascript files -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
