<?php
session_start();
$user = $_SESSION['user'];
if(!$_SESSION['logged']){
	header("Location: login_admin.php");
}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/admin.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Dodaj podatke</title>
</head>
<body>
<center>
	<div id="container">
		<div id="header">
			<img src="slike/RIznica.png">
		</div>
		<div id='navigation'>
			<ul>
				<li><a href='admin_dodaj.php'><span>Dodaj</span></a></li>
				<li><a href='admin_novi_zahtjevi.php'><span>Novi zahtjevi</span></a></li>
				<li><a href='admin_azuriraj.php'><span>Ažuriraj</span></a></li>
				<li><a href='admin_obrisi.php'><span>Obriši</span></a></li>
				<li><a href='admin_transakcije.php'><span>Transakcije</span></a></li>
				<li><a href='admin_poruka.php'><span>Pošalji poruku</span></a></li>
				<li class='last'><a href='logout.php'><span>Odjava</span></a></li>
			</ul>
		</div>
		<div id="mainContent">
			<br>
					</div>
		<div id="footer">
			<p>RIznica - bank of RIteh<br><a href="https://twitter.com/akatonalddrump">Tonald Drump CEO</p>
		</div>
	</div>
</body>
</html>
