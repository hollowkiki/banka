<?php
session_start();
include("includes/mysql_con.php");
include("timeout.php");

if(!$_SESSION['logged1']){
	header("Location: index.php");
}
if($_SESSION['id2']){
	$id = $_SESSION['id2'];
}
$korisnik = !empty($_SESSION['korisnik']) ? $_SESSION['korisnik'] : '';
$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = '0' ");
$numrows= mysqli_num_rows($result);

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Osobni podaci</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/klijent.css">
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-user'></i> ". $korisnik . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href='klijent_pregled.php'><i class="fa fa-cc-visa"></i> <span>Računi</span></a></li>
				<li><a href='klijent_pregled_placanja.php'><i class="fa fa-dollar"></i> <span>Plaćanje</span></a></li>
				<li><a href='klijent_kalkulator.php'><i class="fa fa-calculator"></i> <span>Štedni kalkulator</span></a></li>
				<li class='active'><a href='klijent_podaci.php'><i class="fa fa-address-card"></i> <span>Osobni podaci</span></a></li>
				<?php
					if($numrows) {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope-open'></i> <span>Poruke " . $numrows . "</span></a></li>";
					} else {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope'></i> <span>Poruke</span></a></li>";
					}
				?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
		 </nav>
		<div id="mainPodaci">
			<br>
			<div style='margin-left: 25%;'><font size=3><b>Osobni podaci</b></font></div>
			<br>
			<br>
			<?php
				include("includes/mysql_con.php");
				$result = mysqli_query($con,"SELECT * FROM klijent WHERE id = '$id' ");
				$row = mysqli_fetch_array($result);
				$username = $row['username'];
				$result2 = mysqli_query($con,"SELECT * FROM last_login WHERE username = '$username' ");
				$row2 = mysqli_fetch_array($result2);

				echo "<form action='klijent_azuriraj.php' method='POST'>";
				echo "<div class='row'><div class='col-md-6'><b>OIB: </b></div><div class='col-md-6'>".$row['OIB']."</div></div>";
				echo "<div class='row'><div class='col-md-6'><b>Ime i prezime: </b></div><div class='col-md-6'>".$row['Ime']." ".$row['Prezime']."</div></div>";
				echo "<div class='row'><div class='col-md-6'><b>Datum rođenja: </b></div><div class='col-md-6'>".$row['Datum_rod']."</div></div>";
				echo "<div class='row'><div class='col-md-6'><b>Mjesto poštanskog ureda: </b></div><div class='col-md-6'>".$row['Mjesto_rod']."</div></div>";
				echo "<div class='row'><div class='col-md-6'><b>Zadnja prijava: </b></div><div class='col-md-6'>".$row2['date']." ".$row2['time']."</div></div>";
				echo "<div class='row'><div class='col-md-6'><b>E-adresa: </b></div><div class='col-md-6'><input type='email' name='email' value='".$row['email']."'></div></div>";
				echo "<div class='row'><input type='submit' name='button' class='btn btn-success' value='Pohrani podatke' style='margin-left: 30%;'></div></form>";
				mysqli_close($con);
			?>
			<br>
			<br>
		</div>
		<!-- jQuery & Bootstrap javascript files -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	</div>
</body>
</html>
