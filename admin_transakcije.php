<?php
//http://jqueryui.com/button/
session_start();

$user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';

if(!$_SESSION['logged']){
	header("Location: login_admin.php");
}
$id = !empty($_GET['id']) ? $_GET['id'] : '';
$id2 = !empty($_SESSION['id2']) ? $_SESSION['id2'] : '';
$tid = !empty($_GET['tid']) ? $_GET['tid'] : '';
?>

<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/admin.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Transakcije</title>
</head>
<body>
<center>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
 	 <div class="container-fluid">
   	 <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      <a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-terminal'></i> ". $user . "</p><br><br> "; ?></p></a>


     </div>
     <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
				<li><a href='admin_dodaj.php'><i class="fa fa-check"></i> <span>Dodaj</span></a></li>
				<li><a href='admin_novi_zahtjevi.php'><i class="fa fa-user"></i> <span>Novi zahtjevi</span></a></li>
				<li><a href='admin_azuriraj.php'><i class="fa fa-wrench"></i> <span>Ažuriraj</span></a></li>
				<li><a href='admin_obrisi.php'><i class="fa fa-close"></i> <span>Obriši</span></a></li>
				<li class='active'><a href='admin_transakcije.php'><i class="fa fa-dollar"></i> <span>Transakcije</span></a></li>
				<li><a href='admin_poruka.php'><i class="fa fa-envelope"></i> <span>Pošalji poruku</span></a></li>

   	  </ul>
      <ul class="nav navbar-nav navbar-right">
          <li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
  	  </ul>
     </div>
     </div>
   </nav>
		<div id="mainContent">
			<br>
			<?php
				$con=@mysqli_connect("localhost","root","","banka");
				// check connection
				if (mysqli_connect_errno()) {
					printf("Konekcija neuspjela: %s\n", mysqli_connect_error());
					echo " <a href=klijent_landing.php>Povratak</a>";
					header('Content-Type: text/html; charset=utf-8');
				}
				echo "<table>";

				if($id) {
					$result = mysqli_query($con, "SELECT tid,date FROM transakcija WHERE id1 ='" . $id . "' OR id2 = '" . $id . "' ORDER BY DATE");
					$row2 = mysqli_fetch_array($result);

					if($row2) {
						echo "<tr><th>ID</th><th>Datum</th></tr>";
					} else {
						echo "<p class='textich'><b>Korisnik nema transakcija</b></p>";
					}
					while($row = mysqli_fetch_array($result)) {

						echo "<tr>";
						echo "<td><a href='admin_transakcije.php?tid=" . $row['tid'] . "'>" . $row['tid'] . "</a></td>";
						echo "<td>" . $row['date'] . "</td>";
						echo "</tr>";
					}
					echo "</table>";
				}else if ($tid) {
					$result = mysqli_query($con, "SELECT * FROM transakcija JOIN klijent on id = id1 WHERE tid ='" . $tid . "'");
					$row = mysqli_fetch_array($result);
					echo "<div class='transakcija-text' style='text-align:left; font-size: 15px;'>";
					echo "<b>DATUM:</b> " .$row['date']. "<br>";
					echo "<b>PLATITELJ:</b> " .$row['Ime']. " ".$row['Prezime']. "<br>";
					echo "<b>IBAN:</b> " .$row['IBAN1']. "<br><br>";
					echo "<b>PRIMATELJ:</b> " .$row['ime2']. "<br>";
					echo "<b>IBAN:</b> " .$row['IBAN2']. "<br><br>";
					echo "<b>MODEL:</b> " .$row['model']. "<br>";
					echo "<b>OPIS:</b> " .$row['opis']. "<br>";
					echo "<b>IZNOS:</b> " .$row['iznos']. "<br>";
					echo "<br><br><hr><a href='admin_transakcije.php'>Natrag na transakcije</a>";
					echo "</div>";
				}else {
					echo "<tr><th>OIB</th><th>Ime</th><th>Prezime</th><th>Datum rođenja</th><th>Prebivalište</th><th>Username</th><th>Password</th><th>Email</th></tr>";

					$result = mysqli_query($con, "SELECT * FROM klijent WHERE status = 1 ORDER BY prezime,ime");

					while($row = mysqli_fetch_array($result)) {
						echo "<tr>";
						echo "<td>" . $row['OIB'] . "</td>";
						echo "<td>" . $row['Ime'] . "</td>";
						echo "<td>" . $row['Prezime'] . "</td>";
						echo "<td>" . $row['Datum_rod'] . "</td>";
						echo "<td>" . $row['Mjesto_rod'] . "</td>";
						echo "<td>" . $row['username'] . "</td>";
						echo "<td>" . $row['password'] . "</td>";
						echo "<td>" . $row['email'] . "</td>";
						$_SESSION['id2'] = $row['id'];
						echo "<td><a href='admin_transakcije.php?id=". $row['id'] ."'><button class='btn btn-success'>IZLISTAJ</th></button></a></td>";
						echo "</tr>";
					}

				echo "</table>";
			}
				mysqli_close($con);
			?>
		</div>
	</div>
	<!-- jQuery & Bootstrap javascript files -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
