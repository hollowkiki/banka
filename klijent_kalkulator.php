<?php
session_start();
include("includes/mysql_con.php");
include("timeout.php");

if(!$_SESSION['logged1']){
	header("Location: index.html");
}
if($_SESSION['id2']){
	$id = $_SESSION['id2'];
}

$mid = !empty($_GET['mid']) ? $_GET['mid'] : '';
$broj = !empty($_GET['broj']) ? $_GET['broj'] : '5';
$n = !empty($_GET['n']) ? $_GET['n'] : '1';

$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = '0' ");
$numrows= mysqli_num_rows($result);
$result2 = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = 2 ORDER BY time DESC,date DESC");
$rows = mysqli_num_rows($result2);

$korisnik = !empty($_SESSION['korisnik']) ? $_SESSION['korisnik'] : '';
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/klijent.css">
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-user'></i> ". $korisnik . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href='klijent_pregled.php'><i class="fa fa-cc-visa"></i> <span>Računi</span></a></li>
        <li><a href='klijent_pregled_placanja.php'><i class="fa fa-dollar"></i> <span>Plaćanje</span></a></li>
        <li class='active'><a href='klijent_kalkulator.php'><i class="fa fa-calculator"></i> <span>Štedni kalkulator</span></a></li>
        <li><a href='klijent_podaci.php'><i class="fa fa-address-card"></i> <span>Osobni podaci</span></a></li>
        <?php
          if($numrows) {
            echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope-open'></i> <span>Poruke " . $numrows . "</span></a></li>";
          } else {
            echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope'></i> <span>Poruke</span></a></li>";
          }
        ?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
		 </nav>
     <div style="margin-top: 100px;">
       <center>
       <button class="btn btn-info tablinks" href="javascript:void(0)" onclick="hideTable(event, 'Sted')" id="Open1">Štedni kalkulator</button>
         <button class="btn btn-warning tablinks" href="javascript:void(0)" onclick="hideTable(event, 'Kred')" id="Open2">Kreditni kalkulator</button>
       </center>
     </div>
       <div id="Sted" class="tabcontent">
         <?php

         $iznos = !empty($_GET['iznos']) ? $_GET['iznos'] : 0;
         $vrijeme = !empty($_GET['vrijeme']) ? $_GET['vrijeme'] : 0;
         $kamata = !empty($_GET['kamata']) ? $_GET['kamata'] : 0;
         $godisnji_ulog = !empty($_GET['godisnji_ulog']) ? $_GET['godisnji_ulog'] : 0;

         echo '<form method="GET" attribute="GET" action="kalkulator.php">';
         echo '<div class="row"><div class="col-md-6"><p>Iznos (u kunama): </div><div class="col-md-6"><input type="text" id="iznos" name="iznos"> kn</p></div></div>';
         echo '<div class="row"><div class="col-md-6"><p>Godisnja uplata (u kunama): </div><div class="col-md-6"><input type="text" id="godisnji_ulog" name="godisnji_ulog" value="0"> kn</p></div></div>';
         echo '<div class="row"><div class="col-md-6"><p>Vrijeme štednje (u mjesecima): </div><div class="col-md-6"><input type="text" id="vrijeme" name="vrijeme"> m</p></div></div>';
         echo '<div class="row"><div class="col-md-6"><p>Kamatna stopa (u postotku): </div><div class="col-md-6"><input type="text" id="kamata" name="kamata"> %</p></div></div>';
         echo '<div class="row"><div class="col-md-12"><button type="submit" class="btn btn-success" name="answer" id="answer" value="answer">Izračunajte</button></div></div>';
         echo '</form>';

         if (($iznos && $vrijeme && $kamata) != 0) {
           echo '<br><hr><br>';
           $pocetni_iznos=$iznos;
           $godine = $vrijeme/12;
           $mjeseci =$vrijeme%12;

           for($i=0; $i<$godine; $i++){
             $iznos = $iznos + $kamata*$iznos/100;
             if($i < $godine-1){
               $iznos = $iznos + $godisnji_ulog;
             }
           }
           for($i=0; $i<$mjeseci; $i++){
             $iznos = $iznos + ($kamata/12)*($iznos/100);
           }
           $rjesenje = $iznos;
           echo '<p>Ukupan iznos nakon isteka štednje ('. $vrijeme .' mjeseci): ' . round( $rjesenje, 2, PHP_ROUND_HALF_ODD) . ' kn.<br>';
           echo '<p>Iznos kamate: ' . round( $iznos - $pocetni_iznos - $godisnji_ulog*($godine-1), 2, PHP_ROUND_HALF_ODD) . ' kn.';
         }
       ?>
       </div>
       <div id="Kred" class="tabcontent">
         <?php

         $iznos2 = !empty($_GET['iznos2']) ? $_GET['iznos2'] : 0;
         $vrijeme2 = !empty($_GET['vrijeme2']) ? $_GET['vrijeme2'] : 0;
         $kamata2 = !empty($_GET['kamata2']) ? $_GET['kamata2'] : 0;

         echo '<form method="GET" attribute="GET" action="kalkulator.php">';
         echo '<div class="row"><div class="col-md-6"><p>Kredit (u kunama): </div><div class="col-md-6"><input type="text" id="iznos2" name="iznos2"> kn</p></div></div>';
         echo '<div class="row"><div class="col-md-6"><p>Vrijeme trajanja ukamaćivanja (u godinama): </div><div class="col-md-6"><input type="text" id="vrijeme2" name="vrijeme2"> g</p></div></div>';
         echo '<div class="row"><div class="col-md-6"><p>Kamatna stopa (u postotku): </div><div class="col-md-6"><input type="text" id="kamata2" name="kamata2"> %</p></div></div>';
         echo '<div class="row"><div class="col-md-12"><button type="submit" class="btn btn-success" name="answer" id="answer" value="answer">Izračunajte</button></div></div>';
         echo '</form>';

         if (($iznos2 && $vrijeme2 && $kamata2) != 0) {
           echo '<br><hr>';
           $pocetni_iznos2=$iznos2;
           $kamate=$iznos2*$kamata2/100;
           $nkamate=$kamate*$vrijeme2;
           $iznos2=$pocetni_iznos2 + $nkamate;

           echo '<p>Ukupan iznos nakon isteka kredita ('. $vrijeme2 .' godina): ' . round( $iznos2, 2, PHP_ROUND_HALF_ODD) . ' kn.<br>';
           echo '<p>Iznos godišnjeg anuiteta (bez kamate): ' . round( $iznos2/$vrijeme2 - $kamate, 2, PHP_ROUND_HALF_ODD) . ' kn.';
           echo '<p>Iznos godišnje kamate: ' . round( $kamate, 2, PHP_ROUND_HALF_ODD) . ' kn.';
           echo '<p>Iznos godišnjeg anuiteta: ' . round( $iznos2/$vrijeme2, 2, PHP_ROUND_HALF_ODD) . ' kn.';
           echo '<p>Iznos mjesečnog anuiteta (bez kamate): ' . round( $iznos2/($vrijeme2*12) - $kamate/12, 2, PHP_ROUND_HALF_ODD) . ' kn.';
           echo '<p>Iznos mjesečne kamate: ' . round( $kamate/12, 2, PHP_ROUND_HALF_ODD) . ' kn.';
           echo '<p>Iznos mjesečnog anuiteta: ' . round( $iznos2/($vrijeme2*12), 2, PHP_ROUND_HALF_ODD) . ' kn.';

         }
       ?>
       </div>
     </div>

     <script type="text/javascript">
     function hideTable(var1, tableName) {
         // Declare all variables
         var i, tabcontent, tablinks;

         // Get all elements with class="tabcontent" and hide them
         tabcontent = document.getElementsByClassName("tabcontent");
         for (i = 0; i < tabcontent.length; i++) {
             tabcontent[i].style.display = "none";
         }

         // Get all elements with class="tablinks" and remove the class "active"
         tablinks = document.getElementsByClassName("tablinks");
         for (i = 0; i < tablinks.length; i++) {
             tablinks[i].className = tablinks[i].className.replace(" active", "");
         }

         // Show the current tab, and add an "active" class to the link that opened the tab
         document.getElementById(tableName).style.display = "block";
         var1.currentTarget.className += " active";
     }
     // Get the element with id="defaultOpen" and click on it
     document.getElementById("Open1").click();
     <?php
       // Get the element with id="defaultOpen" and click on it
       if (($iznos && $vrijeme && $kamata) != 0) {
         echo "document.getElementById('Open1').click();";
       }
       if (($iznos2 && $vrijeme2 && $kamata2) != 0) {
         echo "document.getElementById('Open2').click();";
       }
     ?>
     </script>

     <!-- jQuery & Bootstrap javascript files -->
 			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
 			<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
 			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    </div>
  </body>
  </html>
