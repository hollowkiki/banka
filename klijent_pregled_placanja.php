<?php
session_start();
include("includes/mysql_con.php");
include("timeout.php");

$korisnik = !empty($_SESSION['korisnik']) ? $_SESSION['korisnik'] : '';

if(!$_SESSION['logged1']){
	header("Location: index.php");
}
if($_SESSION['id2']){
	$id = $_SESSION['id2'];
}
$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = '0' ");
$numrows= mysqli_num_rows($result);

$racun = !empty($_POST['racun']) ? $_POST['racun'] : '';
if($racun == 'tekuci') {
	$result2 = mysqli_query($con,"SELECT * FROM klijent k JOIN racun r ON k.id = r.id WHERE  k.id = '$id' AND tip = 'T'");
	$row= mysqli_fetch_array($result2);
} else {
	$result2 = mysqli_query($con,"SELECT * FROM klijent k JOIN racun r ON k.id = r.id WHERE  k.id = '$id' AND tip = 'Z'");
	$row= mysqli_fetch_array($result2);
}

$ime1 = $row['Ime'].' '.$row['Prezime'];
$ime2 = !empty($_POST['ime']) ? $_POST['ime'] : '';
$mjesto1 = $row['Mjesto_rod'];
$mjesto2 = !empty($_POST['mjesto']) ? $_POST['mjesto'] : '';
$IBAN1 = $row['IBAN'];
$IBAN2 =  !empty($_POST['ibanprim']) ? $_POST['ibanprim'] : '';
$m = !empty($_POST['model']) ? $_POST['model'] : '';
$poziv1 = !empty($_POST['poziv1']) ? $_POST['poziv1'] : '';
$poziv2 = !empty($_POST['poziv2']) ? $_POST['poziv2'] : '';
$model = $m.$poziv1.$poziv2;
$opis = !empty($_POST['opis']) ? $_POST['opis'] : '';
$iznos = !empty($_POST['iznos']) ? $_POST['iznos'] : '';
$date = !empty($_POST['datum']) ? $_POST['datum'] : '';
$posalji = !empty($_POST['posalji']) ? $_POST['posalji'] : '';

$predlozak = !empty($_POST['predlozak']) ? $_POST['predlozak'] : '';

$result3 = mysqli_query($con,"SELECT * FROM klijent k JOIN racun r ON k.id = r.id WHERE  IBAN = '$IBAN2'");
$row2= mysqli_fetch_array($result3);

$message = 'Poštovani, nije moguće vršiti transakcije između istog računa!';


if($predlozak) {
	if($row2) {
		$result = mysqli_query($con, " INSERT INTO predlozak SET id= '$id', IBAN = '$IBAN2', primatelj = '" . $row2['Ime'].' '.$row2['Prezime'] . "', mjesto = '".$row2['Mjesto_rod']."', model = '$model'");
	} else {
		$result = mysqli_query($con, " INSERT INTO predlozak SET id= '$id', IBAN = '$IBAN2', primatelj = '$ime2', mjesto = '$mjesto2', model = '$model'");
	}
	header('Location: klijent_pregled.php');
}

if($posalji) {
	if($IBAN1 == $IBAN2) {
	echo "<SCRIPT type='text/javascript'>
			alert('$message');
			window.location.href='klijent_placanje.php';
		</SCRIPT>";
	} else if($row2) {
		$result4 = mysqli_query($con, " INSERT INTO transakcija SET
											id1='" . $id . "',
											id2='" . $row2['id'] . "',
											date = '" . $date . "',
											IBAN1='" . $IBAN1 . "',
											IBAN2='" . $IBAN2 . "',
											ime2='" . $row2['Ime'].' '.$row2['Prezime'] . "',
											mjesto2='" . $row2['Mjesto_rod'] . "',
											model='" . $model . "',
											opis='" . $opis . "',
											iznos ='" . $iznos . "',
											stanje11 = 0,
											stanje12 = 0");
	header('Location: klijent_pregled_placanja.php');

	} else {
		$result4 = mysqli_query($con, " INSERT INTO transakcija SET
											id1='" . $id . "',
											id2 = 0,
											date = '" . $date . "',
											IBAN1='" . $IBAN1 . "',
											IBAN2='" . $IBAN2 . "',
											ime2='" . $ime2 . "',
											mjesto2='" . $mjesto2 . "',
											model='" . $model . "',
											opis='" . $opis . "',
											iznos='" . $iznos . "',
											stanje11 = 0,
											stanje12 = 0");
	header('Location: klijent_pregled_placanja.php');
	}
}
$result5 = mysqli_query($con,"SELECT * FROM transakcija WHERE id1 = '$id' AND status = 0");

$result6 = mysqli_query($con,"SELECT * FROM transakcija WHERE id1 = '$id' AND status = 0");
$row6 = mysqli_fetch_array($result6);

function nbsp($broj){
	for($i=0;$i<$broj;$i++){
		echo "&nbsp;";
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Plaćanje</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/klijent.css">
	<script type="text/javascript" src="https://www.sanwebe.com/wp-content/cache/minify/ca5f9.js"></script>

	<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.5.0/highlight.min.js?ver=1.0.0'></script>
	<script type="text/javascript" src="https://www.sanwebe.com/wp-content/cache/minify/13d7d.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript">
     $(document).ready(function() {
        $('#select-all').click(function(event) {
            if(this.checked) {
                $('.checkitem').each(function() {
                    this.checked = true;
                });
            }else{
                $('.checkitem').each(function() {
                    this.checked = false;
                });
            }
        });

    });
	</script>
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-user'></i> ". $korisnik . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href='klijent_pregled.php'><i class="fa fa-cc-visa"></i> <span>Računi</span></a></li>
				<li class='active'><a href='klijent_pregled_placanja.php'><i class="fa fa-dollar"></i> <span>Plaćanje</span></a></li>
				<li><a href='klijent_kalkulator.php'><i class="fa fa-calculator"></i> <span>Štedni kalkulator</span></a></li>
				<li><a href='klijent_podaci.php'><i class="fa fa-address-card"></i> <span>Osobni podaci</span></a></li>
				<?php
					if($numrows) {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope-open'></i> <span>Poruke " . $numrows . "</span></a></li>";
					} else {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope'></i> <span>Poruke</span></a></li>";
					}
				?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
		 </nav>
		<div id="mainContent">
			<br>
			<div style='margin-left: 30%;'><font size=3><b>Računi spremni za plaćanje</b></font></div>
			<br>
			<br>
			<?php
				echo "<table id='tab1'>";
				echo "<tr><th><b>IBAN Platitelja</b></th><th><b>IBAN Primatelja</b></th><th><b>Primatelj</b></th><th><b>Iznos</b></th></tr>";
				echo "<form action='transaction.php' method='POST'>";
				while ($row5 = mysqli_fetch_array($result5)) {
					echo "<tr><td><b>".$row5['IBAN1']."</b></td><td><b>".$row5['IBAN2']."</b></td><td>".$row5['ime2']."</td><td><b>".$row5['iznos']." HRK</b></td><td><input type='checkbox' name='box[]' value='".$row5['tid']."' class='checkitem'></td></tr>";
				}
				echo "</table>";
				if($row6) {
					echo "<br><br><br><div style='margin-left: 30%;'><input type='submit' class='btn btn-success' name='plati' value='Plati'>";
					nbsp(2);
					echo "<input type='submit' class='btn btn-danger' name='obrisi' value='Obriši'>";
					nbsp(2);
					echo "<input type='checkbox' name='select-all' id='select-all'><font size=3>Odaberi sve</font></div>";

					}
				echo "</form>";
				mysqli_close($con);
			?>
			<br>
			<br>
		</div>
		<!-- jQuery & Bootstrap javascript files -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	</div>
</body>
</html>
