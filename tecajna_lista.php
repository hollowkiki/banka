

<!DOCTYPE html>
<html>
<head>
	<title>RIznica - bank of RIteh</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="slike/RIznicaS.png" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
 	 <div class="container-fluid">
   	 <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      <a class="navbar-brand" href="index.php"><img  src="slike/RIznica.png" style="height: 30px"></a>

     </div>
     <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">

				<li><a href='zatrazi_registraciju.php' target="_blank"><span>Registracija</span></a></li>
				<li><a href='kalkulator.php' target="_blank"><span>Kalkulator</span></a></li>
				<li><a href='tecajna_lista.php' target="_blank"><span>Tečajna Lista</span></a></li>
   	  </ul>
      <ul class="nav navbar-nav navbar-right">
          <li><a href='login_klijent.php'><span>Log in</span></a></li>
  	  </ul>
     </div>
     </div>
   </nav>
	<section id="tecaj">
		 	<?php
				$file = fopen("http://www.hnb.hr/hnb-tecajna-lista-portlet/rest/tecajn/getformatedrecords.dat","r");
				$junk = fgets($file, '4');
				$dan = fgets($file, '3');
				$mjesec = fgets($file, '3');
				$godina = fgets($file, '5');
				$dan2 = fgets($file, '3');
				$junk = fgets($file);

				echo "<center><h1>Tečajna lista</h1>";
				echo "<p> Utvrđena na dan " . $dan . "." . $mjesec . "." . $godina .".</p>";
				echo "<p> Tečajevi iz ove liste primjenjuju se od " . $dan2 . "." . $mjesec . "." . $godina .".</p></br></center>";
				echo "<div class='row' style='font-weight: bold'><div class='col-md-2'>Šifra valute</div><div class='col-md-2'>Valuta</div><div class='col-md-2'>Jedinica</div><div class='col-md-2'>Kupovni tečaj</div><div class='col-md-2'>Srednji tečaj</div><div class='col-md-2'>Prodajni tečaj</div></div>";
				while(!feof($file))
  				{
  					echo "<div class='row'>";
  					$sifra_valute = fgets($file, '4');
  					echo "<div class='col-md-2'>" . $sifra_valute . "</div><div class='col-md-2'>";
  					$valuta = fgets($file, '4');
  					switch ($valuta) {
  						case 'AUD':
  							echo "Australski Dolar (" . $valuta . ")</div>";
  							break;
  						case 'CAD':
  							echo "Kanadski Dolar (" . $valuta . ")</div>";
  							break;
  						case 'CZK':
  							echo "Češka Kruna (" . $valuta . ")</div>";
  							break;
  						case 'DKK':
  							echo "Danska Kruna (" . $valuta . ")</div>";
  							break;
  						case 'HUF':
  							echo "Mađarska Forinta (" . $valuta . ")</div>";
  							break;
  						case 'JPY':
  							echo "Japanski Jen (" . $valuta . ")</div>";
  							break;
  						case 'NOK':
  							echo "Norveška Kruna (" . $valuta . ")</div>";
  							break;
  						case 'SEK':
  							echo "Švedska Kruna (" . $valuta . ")</div>";
  							break;
  						case 'CHF':
  							echo "Švicarski Franak (" . $valuta . ")</div>";
  							break;
  						case 'GBP':
  							echo "Britanska Funta (" . $valuta . ")</div>";
  							break;
  						case 'USD':
  							echo "Američki Dolar (" . $valuta . ")</div>";
  							break;
  						case 'EUR':
  							echo "Euro (" . $valuta . ")</div>";
  							break;
  						case 'PLN':
  							echo "Poljski Zloti (" . $valuta . ")</div>";
  							break;

  						default:
  							echo $valuta . "</div>";
  							break;
  					}
  					$jedinica = fgets($file, '4');
  					echo "<div class='col-md-2'>" . $jedinica . "</div>";
  					$kupovni = fgets($file, '16');
  					echo "<div class='col-md-2'>" . $kupovni . "</div>";
  					$srednji = fgets($file, '16');
  					echo "<div class='col-md-2'>" . $srednji . "</div>";
  					$prodajni = fgets($file, '18'). "<br>";
  					echo "<div class='col-md-2'>" . $prodajni . "</div>";
  					echo "</div>";
  				}
				fclose($file);
			?>

	</section>
</div>
	<footer id="footer" style='margin-top: 390px;'>
			<div class="container">
				<div class="row">
					<div class="col-md-6 footer-stil">
						<p>RIznica - bank of RIteh<br><a href="https://twitter.com/akatonalddrump">Tonald Drump CEO</p>
					</div>
				<div class="col-md-6 footer-stil">
					<a href="login_admin.php" target="_blank"><p>Za administratore</p></a>
					</div>
			</div>
		</div>
	</footer>

		<!-- jQuery & Bootstrap javascript files -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
