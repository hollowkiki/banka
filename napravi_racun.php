<?php
session_start();
include("includes/mysql_con.php");

if(!$_SESSION['logged1']){
	header("Location: index.html");
}
$id = !empty($_SESSION['id2']) ? $_SESSION['id2'] : '';
$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = '0' ");
$numrows= mysqli_num_rows($result);
$korisnik = !empty($_SESSION['korisnik']) ? $_SESSION['korisnik'] : '';
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/klijent.css">
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-user'></i> ". $korisnik . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class='active'><a href='klijent_pregled.php'><i class="fa fa-cc-visa"></i> <span>Računi</span></a></li>
				<li><a href='klijent_pregled_placanja.php'><i class="fa fa-dollar"></i> <span>Plaćanje</span></a></li>
				<li><a href='klijent_kalkulator.php'><i class="fa fa-calculator"></i> <span>Štedni kalkulator</span></a></li>
				<li><a href='klijent_podaci.php'><i class="fa fa-address-card"></i> <span>Osobni podaci</span></a></li>
				<?php
					if($numrows) {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope-open'></i> <span>Poruke " . $numrows . "</span></a></li>";
					} else {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope'></i> <span>Poruke</span></a></li>";
					}
				?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
		 </nav>
		<div id="mainNoRacun">
			<br>
			<font size=3><b>Računi</b></font>
			<br>
			<br>

				<?php
					include("includes/mysql_con.php");
					$result = mysqli_query($con,"SELECT * FROM racun WHERE id = '$id' AND tip = 'T'");
					$num1 = mysqli_num_rows($result);
					$result2 = mysqli_query($con,"SELECT * FROM racun WHERE id = '$id' AND tip = 'Z'");
					$num2 = mysqli_num_rows($result2);

					echo "<form action='napravi.php' method='POST'>";
					if(($num1 == 0)&&($num2 == 0)) {
						echo "<b>Napravi tekući </b><input type='checkbox' name='box[]' value='tekuci'><br>";
						echo "<b>Napravi žiro </b><input type='checkbox' name='box[]' value='ziro'>";
					} else if($num1 == 0) {
						echo "<b>Napravi tekući </b><input type='checkbox' name='box[]' value='tekuci'>";
					} else if($num2 == 0) {
						echo "<b>Napravi žiro </b><input type='checkbox' name='box[]' value='ziro'>";
					}
					echo "<br><br><input type='submit' class='btn btn-success' name='podnesi' value='Podnesite zahtjev'>";
					echo "</form>";
					mysqli_close($con);
				?>
			<br>
			<br>
		</div>
		<!-- jQuery & Bootstrap javascript files -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	</div>
</body>
</html>
