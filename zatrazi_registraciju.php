<?php
	$error = !empty($_GET['error']) ? $_GET['error'] : '';

	if($error == 1) {
		echo "<script type='text/javascript'>
				alert('Lozinke se ne podudaraju!\\nPonovite unos podataka.');
			  </script>";
	} else if($error == 2) {
		echo "<script type='text/javascript'>
				alert('Korisnik s unesenim OIB-om već postoji!\\nPonovite unos podataka.');
			  </script>";
	} else if($error == 3) {
		echo "<script type='text/javascript'>
				alert('Korisnik s unesenim username-om već postoji!\\nPonovite unos podataka.');
			  </script>";
	} else if($error == 4) {
		echo "<script type='text/javascript'>
				alert('Korisnik s unesenom email adresom već postoji!\\nPonovite unos podataka.');
			  </script>";
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>RIznica</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/login_klijent.css">
</head>
<body>
    <div class="container">
			<section id="headerLogin">
					<div class="row">
							<a href="index.php"><img id="logoRI" src="slike/RIznica.png" style="width: 480px;"></a>
					</div>
			</section>
        <div id="registracija">
            <form action="registracija.php" method="POST">
							<div class="row linija">
								<div class="col-md-12">
                <font color="blue"><b>Zahtjev za registracijom</b></font>
							</div>
							</div>
							<div class="row linija">
								<div class="col-md-6">
                	<label for="OIB">OIB:</label>
								</div>
								<div class="col-md-6">
								<input type="text" id="OIB" name="OIB" required>
								</div>
							</div>
							<div class="row linija">
								<div class="col-md-6">
								<label for="ime">Ime:</label>
								</div>
								<div class="col-md-6">
								<input type="text" id="ime" name="ime" required>
								</div>
							</div>
							<div class="row linija">
								<div class="col-md-6">
                <label for="prezime">Prezime:</label>
							</div>
							<div class="col-md-6">
		            <input type="text" id="prezime" name="prezime" required>
							</div>
						</div>
							<div class="row linija">
								<div class="col-md-6">
								<label for="datumrod">Datum rođenja:</label>
							</div>
							<div class="col-md-6">
								<input type="text" name="datumrod" placeholder="YYYY-MM-DD" required>
							</div></div>
							<div class="row linija">
								<div class="col-md-6">
								<label for="mjesto">Prebivalište:</label>
							</div>
							<div class="col-md-6">
								<input type="text" name="mjesto" id="mjesto" required>
							</div>
						</div>
							<div class="row linija">
								<div class="col-md-6">
								<label for="username">Korisničko ime:</label>
							</div>
							<div class="col-md-6">
                <input type="text" id="username" name="username" required>
							</div></div>
							<div class="row linija">
								<div class="col-md-6">
                <label for="password">Lozinka:</label>
							</div>
							<div class="col-md-6">
                <input type="password" id="password" name="password" required>
							</div></div>
							<div class="row linija">
								<div class="col-md-6">
								<label for="password2">Potvrdi lozinku:</label>
							</div>
							<div class="col-md-6">
                <input type="password" id="password2" name="password2" required>
              </div></div>
							<div class="row linija">
								<div class="col-md-6">
                <label for="email">Email:</label>
							</div>
							<div class="col-md-6">
                <input type="email" id="email" name="email" required>
							</div></div>
							<div class="row linija">
								<div class="col-md-12">
								<div style="float: right; padding-top: 10px;">
                <input  type="submit" value="Registracija">
							</div></div></div>
            </form>
        </div>
    </div>
		<!-- jQuery & Bootstrap javascript files -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
