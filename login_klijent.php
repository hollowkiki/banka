<?php
session_start();
$up = !empty($_GET['up']) ? $_GET['up'] : '';
$r = !empty($_GET['r']) ? $_GET['r'] : '';

$logged = !empty($_SESSION['logged']) ? $_SESSION['logged'] : '';
$logged1 = !empty($_SESSION['logged1']) ? $_SESSION['logged1'] : '';

if($logged1){
  header("Location: klijent_pregled.php");
}
if($logged){
  header("Location: admin_dodaj.php");
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>RIznica</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/login_klijent.css">
</head>
<body>
    <p style="padding: 10px;">Nemate korisnički račun? Kliknite <a href=zatrazi_registraciju.php>ovdje</a> i zatražite korisnički račun za manje od 3 minute!</p>
        <?php
        if(!$up) {
            echo "<div id='klijent-login'>
              <div class='container'>
                <div class='row'>
                <div id='headerLogin'>
                    <div id='logo'>
                        <a href='index.php'><img id='logoRI' src='slike/RIznica.png' style='width: 480px;'></a>
                    </div>
                </div>
                <form action='login_klijent_verifikacija.php' method='POST'>
                <div class='row'>
                   <div class='col-md-6'>
                    <label for='username'>Korisničko ime:</label>
                   </div>
                   <div class='col-md-6'>
                    <input type='text' id='username' name='username' required>
                   </div>
                </div>
                <div class='row'>
                   <div class='col-md-6'>
                    <label for='password'>Lozinka:</label>
                   </div>
                   <div class='col-md-6'>
                    <input type='password' id='password' name='password' required>
                     </div>
                   </div>
               <div class='row' style='padding-top: 10px; padding-right: 10px;'>
               <div class='col-md-6'>
                     <div id='switchLogin'>
                        <a href='login_admin.php'>
                        <p style='padding: 10px;'>Administrator</p>
                        </a>
                     </div>
                </div>
                 <div class='col-md-6'>
                    <input type='submit' value='Prijava' style='float:right;'>
               </div>
               </div>
               <div class='row' style='padding-right: 10px; float:left;'>
                  <a href='login_klijent.php?up=1'><p style='padding-left: 25px;'>Zaboravljena lozinka?</p></a>
               </div>
                </form>
                </div>
            </div>
            ";
        } else {
            echo "<div id='klijent-login'>
              <div class='container'>
                <div class='row naslov'>
                  <p>U kućicu dolje upišite svoj email s kojim ste registrirani ili svoje korisničko ime. Na Vaš email ćemo poslati link putem kojeg možete promijeniti svoju lozinku.</p>
                </div>
                <form action='napravi.php' method='POST'>
                <div class='row'>
                  <div class='col-md-6'>
                    <input type='text' id='change' name='change'>
                  </div>
                  <div class='col-md-6'>
                    <input type='submit' value='Pošalji'>
                  </div>
                </div>
                </form>

            </div>
            ";
        }
        ?>
    </div>
    <!-- jQuery & Bootstrap javascript files -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
