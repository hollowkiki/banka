<?php
    session_start();
    $kod = !empty($_GET['kod']) ? $_GET['kod'] : '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>RIznica</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/login_klijent.css">
</head>
<body>
    <p style="padding: 10px;">Nemate korisnički račun? Kliknite <a href=zatrazi_registraciju.php>ovdje</a> i zatražite korisnički račun za manje od 3 minute!</p>
    <?php
    if($kod) {
        echo "<div id='klijent-login'>
          <div class='container'>
            <form action='napravi.php' method='POST'>
             <div class='row'>
                <div class='col-md-6'>
                <label for='password1'>Nova lozinka:</label></div>
                <div class='col-md-6'>
                <input type='password' id='password1' name='password1' required></div>
                </div>
                <div class='row'>
                <div class='col-md-6'>
                <label for='password2'>Potvrdi lozinku:</label></div>
                <div class='col-md-6'>
                <input type='password' id='password2' name='password2' required>
                </div></div>
                <div class='row' style='float: right; padding: 10px;'>
                <input type='submit' value='Promijeni'>
                </div>
            </form>
            </div>
        ";
    } else {
        echo "<div id='klijent-login'>
          <div class='container'>
          <div class='row'>
            <p class='naslov'>Upišite kod koji Vam je poslan putem emaila.</p>
          </div>
            <form action='napravi.php' method='POST'>
            <div class='row'>
            <div class='col-md-6'>
                <input type='text' id='kod' name='kod' required>
            </div>
            <div class='col-md-6'>
                <input type='submit' value='Potvrdi'>
            </div>
            </form>
          </div>
        </div>
        ";
    }
    ?>
    </div>
    <!-- jQuery & Bootstrap javascript files -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
