<?php
session_start();

$logged1 = !empty($_SESSION['logged1']) ? $_SESSION['logged1'] : '';
$logged = !empty($_SESSION['logged']) ? $_SESSION['logged'] : '';

$success = !empty($_GET['success']) ? $_GET['success'] : '';
if($success == 1) {
	echo "<script type='text/javascript'>
			alert('Registracija uspješna!\\nHvala vam što koristite naše usluge');
		</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>RIznica - bank of RIteh</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="shortcut icon" href="slike/RIznicaS.png" />
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
 	 <div class="container-fluid">
   	 <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      <a class="navbar-brand" href="index.php"><img  src="slike/RIznica.png" style="height: 30px"></a>

     </div>
     <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">

				<li><a href='zatrazi_registraciju.php' target="_blank"><span>Registracija</span></a></li>
				<li><a href='kalkulator.php' target="_blank"><span>Kalkulator</span></a></li>
				<li><a href='tecajna_lista.php' target="_blank"><span>Tečajna Lista</span></a></li>
   	  </ul>
      <ul class="nav navbar-nav navbar-right">
          <li><a href='login_klijent.php'><span>Log in</span></a></li>
  	  </ul>
     </div>
     </div>
   </nav>
	 <section id="main-content">
	 <div class="row">
		 	<div class="col-md-12 naslov" >
				<br>
				<h1>RIznica bank of RIteh - što možemo učiniti za Vas?</h1>
				<br>
				<br>
				<br>
			</div
		</div>
		<div class="row">
			<div class="col-md-4 funkcija">
					<div class="row">
						<h1 class="title">Fizičke osobe</h1>
					</div>
					<div class="row">
						<a href="zatrazi_registraciju.php" target="_blank"><img class="picture" src="slike/bank.png"></a>
					</div>
					<div class="row">
					<p class="bottom-info">otvorite račun(e) u našoj banci!</p>
				</div>
			</div>
			<div class="col-md-4 funkcija">
					<div class="row">
						<h1 class="title">Tečajna lista</h1>
					</div>
					<div class="row">
						<a href="tecajna_lista.php" target="_blank"><img class="picture" src="slike/company.png"></a>
				</div>
				<div class="row">
					<p class="bottom-info">Dnevno osvježena tečajna lista</p>
				</div>
			</div>
			<div class="col-md-4 funkcija">
					<div class="row">
							<h1 class="title">Kalkulator</h1>
						</div>
					  <div class="row">
								<a href="kalkulator.php" target="_blank"><img class="picture" src="slike/currency.png"></a>
						</div>
						<div class="row">
							<p class="bottom-info">Izračun kamata, projekcije štednje</p>
						</div>
			</div>
		</div>
			<div class="row">
				<h1 id="ispod-funkcija">Otkrijte neobično iskustvo zadovoljnih korisnika bankarskih usluga!</h1>
			</div>
		</div>

	</section>
	</div>
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 footer-stil">
					<p>RIznica - bank of RIteh<br><a href="https://twitter.com/akatonalddrump">Tonald Drump CEO</p>
				</div>
			<div class="col-md-6 footer-stil">
				<a href="login_admin.php" target="_blank"><p>Za administratore</p></a>
				</div>
		</div>
	</div>
	</footer>

	<!-- jQuery & Bootstrap javascript files -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
