<?php
session_start();

$user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';

if(!$_SESSION['logged']){
	header("Location: login_admin.php");
}

$prihvati = !empty($_GET['prihvati']) ? $_GET['prihvati'] : '';
$odbij = !empty($_GET['odbij']) ? $_GET['odbij'] : '';
$odbij2 = !empty($_GET['odbij2']) ? $_GET['odbij2'] : '';
$potvrdi = !empty($_GET['potvrdi']) ? $_GET['potvrdi'] : '';
$id = !empty($_GET['id']) ? $_GET['id'] : '';
$rbr = !empty($_GET['rbr']) ? $_GET['rbr'] : '';
$tip = !empty($_GET['tip']) ? $_GET['tip'] : '';
$var1 = 0;
$var2 = 0;

function poruka($ime,$prezime,$email) {
	require 'PHPMailer/PHPMailerAutoload.php';

	$mail = new PHPMailer;

	$mail->isSMTP();                            // Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';             // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                     // Enable SMTP authentication
	$mail->Username = 'bankariznica@gmail.com';          // SMTP username
	$mail->Password = 'riznicariznica'; // SMTP password
	$mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                          // TCP port to connect to

	$mail->setFrom('bankariznica@gmail.com', 'Banka RIznica');
	//$mail->addReplyTo('info@codexworld.com', 'CodexWorld');
	$mail->addAddress($email);   // Add a recipient
	//$mail->addCC('cc@example.com');
	//$mail->addBCC('bcc@example.com');

	$mail->isHTML(true);  // Set email format to HTML

	$bodyContent = "Poštovani ".$ime." ".$prezime.", Vaša registracija je prihvaćena!";
	//$bodyContent .= '<p>This is the HTML email sent from localhost using PHP script by <b>CodexWorld</b></p>';

	$mail->Subject = 'Potvrda registracije';
	$mail->Body    = $bodyContent;

	if(!$mail->send()) {
	    echo 'Poruka nije uspješno poslana.';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
	    echo 'Poruka je uspješno poslana';
	}
}

// Konekcija za bazu podataka
$con=@mysqli_connect("localhost","root","","banka");
// check connection
if (mysqli_connect_errno()) {
	printf("Konekcija neuspjela: %s\n", mysqli_connect_error());
	echo " <a href=klijent_landing.php>Povratak</a>";
	} else {
		if($odbij) {
			$result2 = mysqli_query($con, "DELETE FROM klijent WHERE id='" . $id . "'");
		} else if ($prihvati) {
			$date = date("d.m.y.");
			$time = date("H:i");
			$title = "Registracija";
			$text = "Poštovani, vaša registracija je prihvaćena!";
			$result1 = mysqli_query($con, "INSERT INTO poruke (id,date,time,title,text,status) VALUES ('$id','$date','$time','$title','$text',0)");
			$result2 = mysqli_query($con, "UPDATE klijent SET status = 1 WHERE id='" . $id . "'");
			$result3 = mysqli_query($con, "SELECT * FROM klijent WHERE id='" . $id . "'");
			$row = mysqli_fetch_array($result3);
			poruka($row['Ime'],$row['Prezime'],$row['email']);
		} else if ($potvrdi) {
			if($tip == 'T') {
				$date = date("d.m.y.");
				$time = date("H:i");
				$title = "Tekući račun";
				$text = "Poštovani, uspješno vam je napravljen tekući račun!";
				$result3 = mysqli_query($con, "INSERT INTO poruke (id,date,time,title,text,status) VALUES ('$id','$date','$time','$title','$text',0)");
				$result4 = mysqli_query($con, "UPDATE racun SET status = 1 WHERE racun_br='" . $rbr . "'");
			} else {
				$date = date("d.m.y.");
				$time = date("H:i");
				$title = "Žiro račun";
				$text = "Poštovani, uspješno vam je napravljen Žiro račun!";
				$result3 = mysqli_query($con, "INSERT INTO poruke (id,date,time,title,text,status) VALUES ('$id','$date','$time','$title','$text',0)");
				$result4 = mysqli_query($con, "UPDATE racun SET status = 1 WHERE racun_br='" . $rbr . "'");
			}
		} else if ($odbij2) {
			$result = mysqli_query($con, "DELETE FROM racun WHERE racun_br='" . $rbr . "'");
		}
}
mysqli_close($con);
?>

<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/admin.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Novi zahtjevi</title>
</head>
<body>
<center>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
 	 <div class="container-fluid">
   	 <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      <a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-terminal'></i> ". $user . "</p><br><br> "; ?></p></a>


     </div>
     <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
				<li><a href='admin_dodaj.php'><i class="fa fa-check"></i> <span>Dodaj</span></a></li>
				<li class='active'><a href='admin_novi_zahtjevi.php'><i class="fa fa-user"></i> <span>Novi zahtjevi</span></a></li>
				<li><a href='admin_azuriraj.php'><i class="fa fa-wrench"></i> <span>Ažuriraj</span></a></li>
				<li><a href='admin_obrisi.php'><i class="fa fa-close"></i> <span>Obriši</span></a></li>
				<li><a href='admin_transakcije.php'><i class="fa fa-dollar"></i> <span>Transakcije</span></a></li>
				<li><a href='admin_poruka.php'><i class="fa fa-envelope"></i> <span>Pošalji poruku</span></a></li>

   	  </ul>
      <ul class="nav navbar-nav navbar-right">
          <li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
  	  </ul>
     </div>
     </div>
   </nav>
		<div id="mainContent">
			<br>
			<?php
				 // Konekcija za bazu podataka
				$con=@mysqli_connect("localhost","root","","banka");

				$result = mysqli_query($con, "SELECT * FROM klijent WHERE status = 0 ORDER BY prezime,ime");
				$row = mysqli_fetch_array($result);

				header('Content-Type: text/html; charset=utf-8');
				

				if($row) {
					echo "<p class='textich'><b>Korisnici</b></p>";
					echo "<table>";
					echo "<tr><th>OIB</th><th>Ime</th><th>Prezime</th><th>Datum rođenja</th><th>Prebivalište</th><th>Username</th><th>Password</th><th>Email</th></tr>";
					$var1 = 1;
				}
			
				// check connection
				if (mysqli_connect_errno()) {
					printf("Konekcija neuspjela: %s\n", mysqli_connect_error());
					echo " <a href=klijent_landing.php>Povratak</a>";
				} else {
					$result = mysqli_query($con, "SELECT * FROM klijent WHERE status = 0 ORDER BY prezime,ime");

					while($row = mysqli_fetch_array($result)) {
						echo "<tr>";
						echo "<td>" . $row['OIB'] . "</td>";
						echo "<td>" . $row['Ime'] . "</td>";
						echo "<td>" . $row['Prezime'] . "</td>";
						echo "<td>" . $row['Datum_rod'] . "</td>";
						echo "<td>" . $row['Mjesto_rod'] . "</td>";
						echo "<td>" . $row['username'] . "</td>";
						echo "<td>" . $row['password'] . "</td>";
						echo "<td>" . $row['email'] . "</td>";
						echo "<td><a href='admin_novi_zahtjevi.php?prihvati=1&id=". $row['id'] ."'><button class='btn btn-success'>PRIHVATI</button></a></td>";
						echo "<td><a href='admin_novi_zahtjevi.php?odbij=1&id=". $row['id'] ."'><button class='btn btn-danger'>ODBIJ</button ></a></td>";
						echo "</tr>";
					}
					echo "</table><br><br>";
				}

				$result2 = mysqli_query($con, "SELECT * FROM klijent k JOIN racun r on k.id = r.id WHERE r.status = 0 ");
				$result3 = mysqli_query($con, "SELECT * FROM klijent k JOIN racun r on k.id = r.id WHERE r.status = 0 ");
				if($row2 = mysqli_fetch_array($result2)) {
					echo "<p class='textich'><b>Računi</b></p>";
					echo "<table>";
					echo "<tr><th>OIB</th><th>Ime</th><th>Prezime</th><th>Tip računa</th><th>IBAN</th></tr>";
					$var2 = 1;
					while($row3 = mysqli_fetch_array($result3)) {
						echo "<tr><td>".$row3['OIB']."</td><td>".$row3['Ime']."</td><td>".$row3['Prezime']."</td><td>".$row3['tip']."</td><td>".$row3['IBAN']."</td><td><a href='admin_novi_zahtjevi.php?potvrdi=1&rbr=". $row3['racun_br'] ."&id=". $row3['id'] ."&tip=". $row3['tip'] ."'><button class='btn btn-success'>POTVRDI</button></a></td><td><a href='admin_novi_zahtjevi.php?odbij2=1&rbr=". $row3['racun_br'] ."'><button class='btn btn-danger'>ODBIJ</button></a></td></tr>";
					}
					echo "</table>";
				}
				if(!$var1 && !$var2) {
					echo "<p class='textich'><b>Trenutno nema registracija korisnika ni zahtjeva za računima</b></p>";
				}
				mysqli_close($con);
			?>
		</div>
	<!-- jQuery & Bootstrap javascript files -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
