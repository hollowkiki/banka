<?php
session_start();

$user = !empty($_SESSION['user']) ? $_SESSION['user'] : '';

if(!$_SESSION['logged']){
	header("Location: login_admin.php");
}
$azuriraj = !empty($_GET['azuriraj']) ? $_GET['azuriraj'] : '';
$id = !empty($_GET['id']) ? $_GET['id'] : '';

$var = (!empty($azuriraj) && !empty($id)) ? 1 : 0;

?>

<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/admin.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Ažuriraj podatke</title>
	<script>
		function confirmation() {
			window.alert("Podaci o klijentu su uspješno izmijenjeni!");
		}
	</script>
</head>
<body>
<center>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
 	 <div class="container-fluid">
   	 <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
      <a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-terminal'></i> ". $user . "</p><br><br> "; ?></p></a>


     </div>
     <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
				<li><a href='admin_dodaj.php'><i class="fa fa-check"></i> <span>Dodaj</span></a></li>
				<li><a href='admin_novi_zahtjevi.php'><i class="fa fa-user"></i> <span>Novi zahtjevi</span></a></li>
				<li class='active'><a href='admin_azuriraj.php'><i class="fa fa-wrench"></i> <span>Ažuriraj</span></a></li>
				<li><a href='admin_obrisi.php'><i class="fa fa-close"></i> <span>Obriši</span></a></li>
				<li><a href='admin_transakcije.php'><i class="fa fa-dollar"></i> <span>Transakcije</span></a></li>
				<li><a href='admin_poruka.php'><i class="fa fa-envelope"></i> <span>Pošalji poruku</span></a></li>
   	  </ul>
      <ul class="nav navbar-nav navbar-right">
          <li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
  	  </ul>
     </div>
     </div>
   </nav>
		<div id="mainContent">
			<br>
			<?php
				header('Content-Type: text/html; charset=utf-8');
				echo "<table>";
				if($var) {

					include("includes/mysql_con.php");

					if (mysqli_connect_errno()) {
						printf("Konekcija neuspjela: %s\n", mysqli_connect_error());
						echo " <a href=klijent_landing.php>Povratak</a>";
					} else {
						$result = mysqli_query($con, "SELECT * FROM klijent WHERE id ='" . $id . "'");
						$row = mysqli_fetch_array($result);
						$oib = $row['OIB'];
						$ime = $row['Ime'];
						$prezime = $row['Prezime'];
						$datum_rodenja = $row['Datum_rod'];
						$mjesto_rodenja = $row['Mjesto_rod'];
						$username = $row['username'];
						$password = $row['password'];
						$email = $row['email'];
						$_SESSION['id'] = $row['id'];

						echo "<form action='update_klijent.php' onsubmit='confirmation()' method='POST'>";
						echo "<tr><td>ID</td><td>" . $id. "</td></tr>";
						echo "<tr><td>OIB</td><td><input type='text' name='OIB' value='" . $oib . "'></td></tr>";
						echo "<tr><td>Ime</td><td><input type='text' name='Ime' value='" . $ime . "'></td></tr>";
						echo "<tr><td>Prezime</td><td><input type='text' name='Prezime' value='" . $prezime . "'></td></tr>";
						echo "<tr><td>Datum rođenja</td><td><input type='text' name='Datum_rod' value='" . $datum_rodenja . "'></td></tr>";
						echo "<tr><td>Prebivalište</td><td><input type='text' name='Mjesto_rod' value='" . $mjesto_rodenja . "'></td></tr>";
						echo "<tr><td>Username</td><td><input type='text' name='username' value='" . $username . "'></td></tr>";
						echo "<tr><td>Password</td><td><input type='text' name='password' value='" . $password . "'></td></tr>";
						echo "<tr><td>Email</td><td><input type='text' name='email' value='" . $email . "'></td></tr>";
						echo "<tr><td><input type='submit' name='button' value='Izmijeni'></td></tr>";
						echo "</form>";
					}
				} else {
				echo "<tr><th>OIB</th><th>Ime</th><th>Prezime</th><th>Datum rođenja</th><th>Prebivalište</th><th>Username</th><th>Password</th><th>Email</th></tr>";

				include("includes/mysql_con.php");

				if (mysqli_connect_errno()) {
					printf("Konekcija neuspjela: %s\n", mysqli_connect_error());
					echo " <a href=klijent_landing.php>Povratak</a>";
				} else {
					$result = mysqli_query($con, "SELECT * FROM klijent WHERE status = 1 ORDER BY prezime,ime");

					while($row = mysqli_fetch_array($result)) {
						echo "<tr>";
						echo "<td>" . $row['OIB'] . "</td>";
						echo "<td>" . $row['Ime'] . "</td>";
						echo "<td>" . $row['Prezime'] . "</td>";
						echo "<td>" . $row['Datum_rod'] . "</td>";
						echo "<td>" . $row['Mjesto_rod'] . "</td>";
						echo "<td>" . $row['username'] . "</td>";
						echo "<td>" . $row['password'] . "</td>";
						echo "<td>" . $row['email'] . "</td>";
						echo "<td><a href='admin_azuriraj.php?azuriraj=1&id=". $row['id'] ."'><button class='btn btn-success'>AŽURIRAJ</button></a></td>";
						echo "</tr>";
					}
				}
			}
				echo "</table>";
				mysqli_close($con);
			?>
		</div>
	</div>
</body>
</html>
