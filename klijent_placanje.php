<?php
session_start();
include("includes/mysql_con.php");
include("timeout.php");

if(!$_SESSION['logged1']){
	header("Location: index.php");
}
if($_SESSION['id2']){
	$id = $_SESSION['id2'];
}
$korisnik = !empty($_SESSION['korisnik']) ? $_SESSION['korisnik'] : '';
function nbsp($n) {
	for($i=0;$i<$n;$i++)
		echo "&nbsp;";
}

$action = !empty($_GET['a']) ? $_GET['a'] : '';
$select = !empty($_GET['select']) ? $_GET['select'] : '';

if($select) {
	$result = mysqli_query($con,"SELECT * FROM predlozak WHERE pid = '$select'  ");
	$row = mysqli_fetch_array($result);

	$iban = $row['IBAN'];
	$primatelj = $row['primatelj'];
	$mjesto = $row['mjesto'];
} else {
	$iban = "";
	$primatelj = "";
	$mjesto = "";
}

$result = mysqli_query($con,"SELECT * FROM racun WHERE id = '$id' AND tip = 'T' and status = 1 ");
$row = mysqli_fetch_array($result);

$result2 = mysqli_query($con,"SELECT * FROM racun WHERE id = '$id' AND tip = 'Z' and status = 1 ");
$row2 = mysqli_fetch_array($result2);

$result3 = mysqli_query($con,"SELECT * FROM klijent WHERE id = '$id'");
$row3 = mysqli_fetch_array($result3);

$result4 = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = '0' ");
$numrows= mysqli_num_rows($result4);

$res= mysqli_query($con,"SELECT * FROM predlozak WHERE id = '$id'");


?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/klijent.css">
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-terminal'></i> ". $korisnik . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class='active'><a href='klijent_pregled.php'><span>Računi</span></a></li>
				<li><a href='klijent_pregled_placanja.php'><span>Plaćanje</span></a></li>
				<li><a href='klijent_kalkulator.php'><span>Štedni kalkulator</span></a></li>
				<li><a href='klijent_podaci.php'><span>Osobni podaci</span></a></li>
				<?php
					if($numrows) {
						echo "<li><a href='klijent_poruke.php'><span>Poruke " . $numrows . "</span></a></li>";
					} else {
						echo "<li><a href='klijent_poruke.php'><span>Poruke</span></a></li>";
					}
				?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class='last'><a href='logout.php'><span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
		 </nav>
		<div id="mainContent">
			<br>
			<font size=4><b>PLAĆANJE</b></font>
			<br>
			<br>
			<?php


			echo "<font size=3><b>Platitelj</b></font><hr><br>";
			echo "<div class='row info-text'><div class='col-md-6'><select name='racun' form='slanje'>";
			if ($action == 't') {
			if($row)
				echo "<option value='tekuci' selected='selected'>Moj tekući [" . $row['saldo'] . " kn]</option>";
			else if($row2)
				echo "<option value='ziro'>Moj žiro [" . $row2['saldo'] . " kn]</option>";
			else if($row && $row2)
				echo "<option value='tekuci' selected='selected'>Moj tekući [" . $row['saldo'] . " kn]</option>";
				echo "<option value='ziro'>Moj žiro [" . $row2['saldo'] . " kn]</option>";
			echo "</select></div>";

			echo "<form action='klijent_placanje.php' method='GET'>";
			echo "<div class='col-md-4'><b>Predlošci: </b>";
			echo "<select name='select'>";
			echo "<option value='0' selected='selected'></option>";
			while ($red = mysqli_fetch_array($res)) {
				echo "<option value='".$red['pid']."'>".$red['primatelj']."</option>";
			}
			echo "</select></div>";
			echo "<div class='col-md-2'><input type='hidden' name='a' value='t'>";
			echo "<input type='submit' name'odabir' value='Odaberi'></div></div>";
			echo "</form>";

			} else {
			if($row)
				echo "<option value='tekuci'>Moj tekući [" . $row['saldo'] . " kn]</option>";
			else if($row2)
				echo "<option value='ziro' selected='selected'>Moj žiro [" . $row2['saldo'] . " kn]</option>";
			else if($row && $row2)
				echo "<option value='tekuci'>Moj tekući [" . $row['saldo'] . " kn]</option>";
				echo "<option value='ziro' selected='selected'>Moj žiro [" . $row2['saldo'] . " kn]</option>";
			echo "</select>";

			echo "<form action='klijent_placanje.php' method='GET'>";
			echo "<b>Predlošci: </b>";
			echo "<select name='select'>";
			echo "<option value='0' selected='selected'></option>";
			while ($red = mysqli_fetch_array($res)) {
				echo "<option value='".$red['pid']."'>".$red['primatelj']."</option>";
			}
			echo "</select>";
			echo "<input type='hidden' name='a' value='z'>";
			echo "<input type='submit' name'odabir' value='Odaberi'>";
			echo "</form>";

			}

			echo "<div class='info-text'></br><b>Platitelj: </b>". $row3['Ime'] ." ". $row3['Prezime'] ."<br>";
			echo "<b>Mjesto: </b>". $row3['Mjesto_rod']  ."<br>";

			echo "<form id='slanje' action='klijent_pregled_placanja.php' method='POST'>";
			echo "</br></br><br><br><b>Primatelj</b><hr><br>";
			echo "<div class='row'><div class='col-md-6'><b>Račun primatelja/IBAN: </b></div><div class='col-md-6'><input type='text' name='ibanprim' value='".$iban."' required></div></div>";
			echo "<div class='row'><div class='col-md-6'><b>Model i poziv na broj: </b></div><div class='col-md-6'><select name='model'><option selected='selected'>HR</option></select>";
			echo "<input type='number' name='poziv1' required><input type='number' name='poziv2' required></div></div>";
			echo "<div class='row'><div class='col-md-6'><b>Primatelj: </b></div><div class='col-md-6'><input type='text' name='ime' value='".$primatelj."' required></div></div>";
			echo "<div class='row'><div class='col-md-6'><b>Mjesto: </b></div><div class='col-md-6'><input type='text' name='mjesto' value='".$mjesto."' required></div></div>";

			echo "<br><br><br><b>Plaćanje</b><hr><br>";
			echo "<b>Opis plaćanja: </b><br><textarea name='opis' cols='100' rows='5' required></textarea><br>";
			echo "<b>Iznos: </b><input type='text' name='iznos' required> <b>Datum: </b><input type='date' name='datum' required><br><br>";
			echo "<input type='submit' name='posalji' class='btn btn-success' value='Spremi za plaćanje'>&nbsp;&nbsp;&nbsp;";
			echo "<input type='submit' name='predlozak' class='btn btn-warning' value='Spremi kao predložak'>";
			echo "</form></div>";
			mysqli_close($con);
			?>
			<br>
			<br>
		</div>
		<!-- jQuery & Bootstrap javascript files -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	</div>
</body>
</html>
