<?php
session_start();
include("includes/mysql_con.php");

if(!$_SESSION['logged1']){
	header("Location: index.html");
}
$id = !empty($_SESSION['id2']) ? $_SESSION['id2'] : '';
$action = !empty($_GET['a']) ? $_GET['a'] : '';
$tid = !empty($_GET['tid']) ? $_GET['tid'] : '';
$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = '0' ");
$numrows= mysqli_num_rows($result);
$korisnik = !empty($_SESSION['korisnik']) ? $_SESSION['korisnik'] : '';
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/klijent.css">
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-user'></i> ". $korisnik . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li class='active'><a href='klijent_pregled.php'><i class="fa fa-cc-visa"></i> <span>Računi</span></a></li>
				<li><a href='klijent_pregled_placanja.php'><i class="fa fa-dollar"></i> <span>Plaćanje</span></a></li>
				<li><a href='klijent_kalkulator.php'><i class="fa fa-calculator"></i> <span>Štedni kalkulator</span></a></li>
				<li><a href='klijent_podaci.php'><i class="fa fa-address-card"></i> <span>Osobni podaci</span></a></li>
				<?php
					if($numrows) {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope-open'></i> <span>Poruke " . $numrows . "</span></a></li>";
					} else {
						echo "<li><a href='klijent_poruke.php'><i class='fa fa-envelope'></i> <span>Poruke</span></a></li>";
					}
				?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
		 </nav>
		<div id="mainContent">
			<br>
			<font size=3><b>Računi</b></font>
			<br>
			<br>
				<?php
					include("includes/mysql_con.php");
					if ($tid) {
						$result = mysqli_query($con,"SELECT * FROM transakcija t JOIN racun r on t.id1 = id JOIN klijent k on t.id1 = k.id  WHERE tid = '$tid' ");
						$row = mysqli_fetch_array($result);
						echo "<div class='row info-transakcija'>";
						echo "<div class='col-md-6'><div class='row'><div class='col-md-12'><b>Platitelj</b><hr></div></div>";
						echo "<div class='row'><div class='col-md-6'><b>Račun platitelja: </b></div><div class='col-md-6'>".$row['IBAN1']."</div></div>";
						echo "<div class='row'><div class='col-md-6'><b>Ime platitelja: </b></div><div class='col-md-6'>".$row['Ime']." ".$row['Prezime']."</div></div>";
						echo "<div class='row'><div class='col-md-6'><b>Model i poziv na broj: </b></div><div class='col-md-6'>".$row['model']."</div></div></div>";
						echo "<div class='col-md-6'>";
						echo "<div class='row'><div class='col-md-12'><b>Primatelj</b><hr></div></div>";
						echo "<div class='row'><div class='col-md-6'><b>Račun primatelja: </b></div><div class='col-md-6'>".$row['IBAN2']."</div></div>";
						echo "<div class='row'><div class='col-md-6'><b>Ime primatelja: </b></div><div class='col-md-6'>".$row['ime2']."</div></div>";
						echo "<div class='row'><div class='col-md-6'><b>Mjesto: </b></div><div class='col-md-6'>".$row['mjesto2']."</div></div>";
						echo "</div></div>";
						echo "<div class='row detalji-transakcije'>";
						echo "</br></br><div class='row'><div class='col-md-12'><b>Detalji transakcije</b><hr></div></div>";
						echo "<div class='row'><div class='col-md-6 levo'><b>Opis plaćanja: </b></div><div class='col-md-6'>".$row['opis']."</div></div>";
						echo "<div class='row'><div class='col-md-6 levo'><b>Datum: </b></div><div class='col-md-6'>".$row['date']."</div></div>";
						echo "<div class='row'><div class='col-md-6 levo'><b>Iznos: </b></div><div class='col-md-6'>".$row['iznos']." kn</div></div>";
						echo "</div>";
						echo "</br></br><div class='row' style='padding-left: 40%;'>";
						echo "<a href='klijent_pregled.php'><button class='btn btn-danger'>Zatvori</button></a>&nbsp;&nbsp;&nbsp;&nbsp;";
						echo "<a href='pdf.php?tid=".$tid."' target='_blank'><button class='btn btn-warning'>Preuzmi kao PDF</button></a>";
						echo "</div>";
					}
					else if($action == 't') {
						$result = mysqli_query($con,"SELECT * FROM racun WHERE id = '" . $id . "' AND tip = 'T'");
						$row = mysqli_fetch_array($result);
						echo "<div class='info-text'><b>IBAN: </b>".$row['IBAN']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>STANJE: </b>".$row['saldo']." kn<br><br></div>";
						echo "<table style='font-size: 15px;'>";
						echo "<tr><th>DATUM</th><th>SVRHA</th><th>ISPLATA</th><th>UPLATA</th><th>STANJE</th></tr>";
						$result2 = mysqli_query($con,"SELECT * FROM transakcija t JOIN racun r on t.IBAN1 = r.IBAN WHERE t.IBAN1 = '".$row['IBAN']."' AND t.status = 1 UNION DISTINCT SELECT * FROM transakcija t JOIN racun r on t.IBAN2 = r.IBAN WHERE t.IBAN2 = '".$row['IBAN']."' AND t.status = 1 ORDER BY date DESC");
						while($row2 = mysqli_fetch_array($result2)) {
							if($row2['IBAN1'] == $row['IBAN'] && $row2['tip'] == 'T') {
								echo "<tr><td>" .$row2['date']. "</td><td>" .$row2['opis']. "</td><td>-" .$row2['iznos']. " kn</td><td></td><td>" .$row2['stanje11']. " kn</td><td><a href='klijent_transakcije?tid=". $row2['tid']. "'><button class='btn btn-info'>Detalji</button></a></td></tr>";
							} else if($row2['IBAN2'] == $row['IBAN'] && $row['tip'] == 'T') {
								echo "<tr><td>" .$row2['date']. "</td><td>" .$row2['opis']. "</td><td></td><td>" .$row2['iznos']. " kn</td><td>" .$row2['stanje12']. " kn</td><td><a href='klijent_transakcije?tid=". $row2['tid']. "'><button class='btn btn-info'>Detalji</button></a></td></tr>";
							}
						}
					} else {
						$result = mysqli_query($con,"SELECT * FROM racun WHERE id = '" . $id . "' AND tip = 'Z'");
						$row = mysqli_fetch_array($result);
						echo "<div class='info-text'><b>IBAN: </b>".$row['IBAN']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>STANJE: </b>".$row['saldo']." kn<br><br>";
						echo "</div><table style='font-size: 15px;'>";
						echo "<tr><th>DATUM</th><th>SVRHA</th><th>ISPLATA</th><th>UPLATA</th><th>STANJE</th></tr>";
						$result2 = mysqli_query($con,"SELECT * FROM transakcija t JOIN racun r on t.IBAN1 = r.IBAN WHERE t.IBAN1 = '".$row['IBAN']."' AND t.status = 1 UNION DISTINCT SELECT * FROM transakcija t JOIN racun r on t.IBAN2 = r.IBAN WHERE t.IBAN2 = '".$row['IBAN']."' AND t.status = 1 ORDER BY date DESC");
						while($row2 = mysqli_fetch_array($result2)) {
							if($row2['IBAN1'] == $row['IBAN'] && $row2['tip'] == 'Z') {
								echo "<tr><td>" .$row2['date']. "</td><td>" .$row2['opis']. "</td><td>-" .$row2['iznos']. " kn</td><td></td><td>" .$row2['stanje11']. " kn</td><td><a href='klijent_transakcije?tid=". $row2['tid']. "'><button class='btn btn-info'>Detalji</button></a></td></tr>";
							} else if($row2['IBAN2'] == $row['IBAN'] && $row['tip'] == 'Z') {
								echo "<tr><td>" .$row2['date']. "</td><td>" .$row2['opis']. "</td><td></td><td>" .$row2['iznos']. " kn</td><td>" .$row2['stanje12']. " kn</td><td><a href='klijent_transakcije?tid=". $row2['tid']. "'><button class='btn btn-info'>Detalji</button></a></td></tr>";
							}
						}
					}
					mysqli_close($con);
				?>
			</table>
			<br>
			<br>
		</div>
		<!-- jQuery & Bootstrap javascript files -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	</div>
</body>
</html>
