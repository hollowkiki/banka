<?php

if( $_SESSION['last_activity'] < time()-$_SESSION['expire_time'] ) {
    header('Location: logout.php'); 
} else{ //if we haven't expired:
    $_SESSION['last_activity'] = time(); 
}

?>