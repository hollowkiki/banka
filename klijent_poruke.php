<?php
session_start();
include("includes/mysql_con.php");
include("timeout.php");

if(!$_SESSION['logged1']){
	header("Location: index.html");
}
if($_SESSION['id2']){
	$id = $_SESSION['id2'];
}
$korisnik = !empty($_SESSION['korisnik']) ? $_SESSION['korisnik'] : '';

$a = !empty($_GET['a']) ? $_GET['a'] : 1;
$broj = !empty($_GET['broj']) ? $_GET['broj'] : '2';
$n = !empty($_GET['n']) ? $_GET['n'] : '1';
$mid = !empty($_GET['mid']) ? $_GET['mid'] : '';
//$stranica = !empty($_GET['stranica']) ? $_GET['stranica'] : '1';

$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status = '0' ");
$numrows= mysqli_num_rows($result);
$result2 = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status IN (0,1) ORDER BY time DESC,date DESC ");
$rows = mysqli_num_rows($result2);

if ($rows / $broj <= 1) {
	$stranica = 1;
}
else if(($rows / $broj > 1) ) {
	if ($rows % $broj != 0) {
		$stranica = (int)($rows/$broj) + 1;
	} else {
		$stranica = (int)($rows/$broj) ;
	}
}
if($rows == 1) {
	$pocetak = 1;
} else {
	$pocetak = $broj * ($n-1) ;
}
function nbsp($broj){
	for($i=0;$i<$broj;$i++){
		echo "&nbsp;";
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Inbox</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/klijent.css">
	<script type="text/javascript" src="https://www.sanwebe.com/wp-content/cache/minify/ca5f9.js"></script>
	<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.5.0/highlight.min.js?ver=1.0.0'></script>
	<script type="text/javascript" src="https://www.sanwebe.com/wp-content/cache/minify/13d7d.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript">
     $(document).ready(function() {
        $('#select-all').click(function(event) {
            if(this.checked) {
                $('.checkitem').each(function() {
                    this.checked = true;
                });
            }else{
                $('.checkitem').each(function() {
                    this.checked = false;
                });
            }
        });

    });
	</script>
</head>

<body>
	<div class="container">
		<nav class="navbar navbar-custom navbar-inverse navbar-fixed-top">
	 <div class="container-fluid">
		 <div class="navbar-header">
				 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				 </button>
			<a class="navbar-brand" href="admin_dodaj.php"><p><?php echo " <p><i class='fa fa-user'></i> ". $korisnik . "</p><br><br> "; ?></p></a>


		 </div>
		 <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href='klijent_pregled.php'><i class="fa fa-cc-visa"></i> <span>Računi</span></a></li>
				<li><a href='klijent_pregled_placanja.php'><i class="fa fa-dollar"></i> <span>Plaćanje</span></a></li>
				<li><a href='klijent_kalkulator.php'><i class="fa fa-calculator"></i> <span>Štedni kalkulator</span></a></li>
				<li><a href='klijent_podaci.php'><i class="fa fa-address-card"></i> <span>Osobni podaci</span></a></li>
				<?php
					if($numrows) {
						echo "<li class='active'><a href='klijent_poruke.php'><i class='fa fa-envelope-open'></i> <span>Poruke " . $numrows . "</span></a></li>";
					} else {
						echo "<li class='active'><a href='klijent_poruke.php'><i class='fa fa-envelope'></i> <span>Poruke</span></a></li>";
					}
				?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class='last'><a href='logout.php'><i class="fa fa-power-off"></i> <span>Odjava</span></a></li>
			</ul>
		 </div>
		 </div>
		 </nav>
		<div id="mainPoruke">
			<br>
			<div style='margin-left: 30%;'><font size=3><b>Poruke</b></font></div>
			<br>
			<br>
				<?php
					include("includes/mysql_con.php");
					if($mid) {
						$result = mysqli_query($con,"UPDATE poruke SET status='1' WHERE mid = '" . $mid ."'");

						$result2 = mysqli_query($con,"SELECT * FROM poruke WHERE mid = '$mid' ");
						$row2 = mysqli_fetch_array($result2);
						echo "<div id='poruka-header'>";
						echo "<div class='row'><div class='col-md-6'><b>NASLOV:</b></div><div class='col-md-6'> ". $row2['title'] ."</div></div>";
						echo "<div class='row'><div class='col-md-6'><b>ŠALJE:</b></div><div class='col-md-6'> Banka RIznica </div></div>";
						echo "<div class='row'><div class='col-md-6'><b>DATUM:</b></div><div class='col-md-6'> ". $row2['date'] ."</div></div>";
						echo "<div class='row'><div class='col-md-6'><b>VRIJEME:</b></div><div class='col-md-6'> ". $row2['time'] ."</div></div></div><hr></br>";
						echo "<div style='padding 10px;'>" . $row2['text'] . "</div>";
						echo "<br><br><hr><a href='klijent_poruke.php'>Natrag na poruke</a>";
					} else {
						echo"<b>Inbox</b> &nbsp;&nbsp;&nbsp; <a href='klijent_trash.php'><button class='btn btn-info'>Trash</button></a><br><br>";
						echo "<table border=0>";
						echo "<tr><th><b>Šalje</b></th><th><b>Datum primitka</b></th><th><b>Naslov</b></th></tr>";
						if($rows == 1) {
							$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status IN (0,1) ORDER BY time DESC,date DESC");
						} else {
							$result = mysqli_query($con,"SELECT * FROM poruke WHERE id = '$id' AND status IN (0,1) ORDER BY date DESC,time DESC LIMIT $pocetak , $broj ");
						}
						while($row = mysqli_fetch_array($result)) {
							echo "<form action='trash_poruke.php' method='POST'>";
							echo "<tr><td>Banka RIznica</td><td>" . $row['date'] . " " . $row['time'] . "</td><td><a href='klijent_poruke.php?mid=" . $row['mid'] . "'>" . $row['title'] ."</a></td><td><input type='checkbox'name='box[]' value='" . $row['mid'] . "'class='checkitem'></td></tr>";
						}
						echo "<input type='submit' class='btn btn-danger' name='obrisi' value='Obriši'>";
						echo nbsp(3)."<input type='checkbox' name='select-all' id='select-all'>Odaberi sve<br><br>";
						echo "</form>";
						echo "</table>";
					}
					mysqli_close($con);
				?>
			<br>
			<br>
			<?php
				echo "<div style='margin-left: 30%;'>";
				if($stranica == 1) {
					echo "1";
				} else {
					if($n>=2) {
						echo "<a href='klijent_poruke.php?n=".($n-1)."'><button class='btn btn-info'>Prethodna</button> </a>";
					}
					for($i=1;$i<=$stranica;$i++) {
						if($i == $n) {
							echo $i;
						} else {
							echo "<a href='klijent_poruke.php?n=".$i."'> <button class='btn btn-info'>".$i."</button> </a>";
						}
					}
					if($n<$stranica) {
						echo "<a href='klijent_poruke.php?n=".($n+1)."'> <button class='btn btn-info'>Sljedeća</button></a>";
					}
				}
				echo "</div>";
			?>
		</div>
		<!-- jQuery & Bootstrap javascript files -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	</div>
</body>
</html>
