<?php
session_start();
$logged = !empty($_SESSION['logged']) ? $_SESSION['logged'] : '';
$logged1 = !empty($_SESSION['logged1']) ? $_SESSION['logged1'] : '';

if($logged1){
  header("Location: klijent_pregled.php");
}
if($logged){
  header("Location: admin_dodaj.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>RIznica</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/login_admin.css">
</head>
<body>
    <div class="container">
        <div id="headerLogin">
            <div id="logo">
                <a href="index.php"><img id="logoRI" src="slike/RIznica.png" style="width: 480px;"></a>
            </div>
        </div>
        <div id="admin-login">
            <form action="login_admin_verifikacija.php" method="POST">
              <div class="row">
                <div class="col-md-6">
                <label for="username">Korisničko ime:</label>
                </div>
              <div class="col-md-6">
                <input type="text" id="username" name="username" required>
              </div>
            </div>
              <div class="row">
                <div class="col-md-6">
                <label for="password">Lozinka:</label>
              </div>
              <div class="col-md-6">
                <input type="password" id="password" name="password" required>
              </div>
              </div>
              <div class="row" style="padding: 10px;">
                <div class="col-md-6">
                      <a href="login_klijent.php">
                          <p style="padding-left: 10px;">Za klijente</p>
                      </a>
                  </div>
                <div class="col-md-6">
                  <input type="submit" value="Prijava" style="float: right;">
                </div>
              </div>
            </form>
        </div>
    </div>
    <!-- jQuery & Bootstrap javascript files -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script-->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
